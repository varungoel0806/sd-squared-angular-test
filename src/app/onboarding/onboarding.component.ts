import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Joining } from './../models/joining';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.css']
})
export class OnboardingComponent implements OnInit {
  private joinings: Joining[];
  // Hard coded settings used for auto-insertion of data
  private MAX_JOININGS_LENGTH = 8;
  private JOININGS_TIME = 1000;

  constructor(private http: Http) { }

  ngOnInit() {
    this.getJoinings().subscribe((data) => {
      this.joinings = data;

      // Start auto insertion of rows
      this.autoInsertData();
    }, (err) => {
      // TODO: Implement error handling as per project guidelines e.g. toastr
      console.log(err);
    });
  }

  /**
   * **********************
   * Data Access
   * **********************
   */

  /**
   * Responsible to get data from mock json file
   * @return Promise object
   */
  getJoinings(): Observable<any> {
    return this.http.get('./data/joinings.json')
      .map((res: any) => res.json())
      .catch((error: any) => error);
  }

  /**
   * **********************
   * PRIVATE FUNCTIONS
   * **********************
   */

  /**
   * Function to auto insert upto MAX_JOININGS_LENGTH rows in joinings table every JOININGS_TIME ms
   */
  autoInsertData() {
    let autoInsertInterval = setInterval(() => {
      if (this.joinings.length === this.MAX_JOININGS_LENGTH) {
        clearInterval(autoInsertInterval);
        return;
      }
      // Push the new entry to array
      this.insertJoining();
    }, this.JOININGS_TIME);
  }

  /**
   * Contains logic for joining object push
   */
  insertJoining() {
    let lastJoining: Joining = this.joinings[this.joinings.length - 1];
    let newJoiningDate = new Date(lastJoining.joiningDate);

    // Add 1 day to the joining date
    newJoiningDate.setDate(new Date(lastJoining.joiningDate).getDate() + 1);

    this.joinings = [...this.joinings, {
      name: lastJoining.name,
      joiningDate: newJoiningDate,
      age: lastJoining.age
    }];
  }

}
