/**
 * Joining Class
 * =============
 *
 * This is to encapsulate all joining related objects
 */
export class Joining {
  name: string;
  joiningDate: Date;
  age: number;
}
